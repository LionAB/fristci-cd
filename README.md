# Exemple de CI/CD avec Gitlab d'une react app

## CI/CD

<h4>1 . Installation des Runners</h4>
     Commande pour installer les runners si non installer </br>
    <code>sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64" </code>
    </br>

<h4> 2 . Autoriser le Runner</h4>
<code>sudo chmod +x /usr/local/bin/gitlab-runner</code></br>

<h4>3. Enregistrer le Runner</h4>
    <code>sudo gitlab-runner register</code></br>
    <code>sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN</code></br>
<h4> 4. Vérifier le statut </h4>
    <code>sudo gitlab-runner verify</code></br>
 
<h4> 5. Demarrer le Runner</h4>
<code>sudo gitlab-runner run</code>


